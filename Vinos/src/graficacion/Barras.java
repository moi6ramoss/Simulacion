package graficacion;
import javax.swing.JFrame;
import org.jfree.chart.*;
import org.jfree.data.category.*;
import org.jfree.chart.plot.*;
/**
 *
 * @author mramoss
 */
public class Barras {
    
    
    public void graficar(double montoUno,double montoDos, double montoTres){
        DefaultCategoryDataset data = new  DefaultCategoryDataset();
        final String vinoUno = "Opcion Uno";
        final String vinoDos = "Opcion Dos";
        final String vinoTres = "Opcion Tres";
        
        data.addValue(montoUno, vinoUno, "Utilidad $");
        data.addValue(montoDos, vinoDos, "Utilidad $");
        data.addValue(montoTres, vinoTres, "Utilidad $");
        JFreeChart grafica = 
                ChartFactory.createBarChart3D("Utilidad en $ de 3 Opciones",
                "Vinos",
                "Utilidad", 
                data,
                PlotOrientation.VERTICAL,true,true,false );
        
        ChartPanel contenedor = new ChartPanel(grafica);
        JFrame ventana = new JFrame("Grafica Utilidad Tres Opciones");
        ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ventana.add(contenedor);
        ventana.setSize(600,500);
        ventana.setVisible(true);
        ventana.setLocationRelativeTo(null);
             
    }
    
}

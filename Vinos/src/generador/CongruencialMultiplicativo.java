/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generador;

import java.util.ArrayList;

/**
 *
 * @author mramoss
 */
public class CongruencialMultiplicativo {
    private double numeroA;
    private double numeroXn;
    private double numeroM;
    private double numeroXo;

    public CongruencialMultiplicativo(double numeroA, double numeroXn,
            double numeroM, double numeroXo) {
        this.numeroA = numeroA;
        this.numeroXn = numeroXn;
        this.numeroM = numeroM;
        this.numeroXo = numeroXo;
    }

    public double getNumeroA() {
        return numeroA;
    }

    public void setNumeroA(double numeroA) {
        this.numeroA = numeroA;
    }

    public double getNumeroXn() {
        return numeroXn;
    }

    public void setNumeroXn(double numeroXn) {
        this.numeroXn = numeroXn;
    }

    public double getNumeroM() {
        return numeroM;
    }

    public void setNumeroM(double numeroM) {
        this.numeroM = numeroM;
    }

    public double getNumeroXo() {
        return numeroXo;
    }

    public void setNumeroXo(double numeroXo) {
        this.numeroXo = numeroXo;
    } 
    
    public ArrayList<Double> generarMultiplicativo() {

        Double numeroNmasUno = 0.0;
        double numeroNn = numeroXo;
        ArrayList<Double> Xn = new ArrayList<>();
        ArrayList<Double> nroAleatorio = new ArrayList<>();
        
        for (int n = 0; n < 4; n++) {
            if (n == 0) {
                numeroNmasUno = (numeroA * numeroXo) % numeroM;
                Xn.add(numeroNn);
                nroAleatorio.add(numeroNmasUno / numeroM);
            } else {
                numeroNmasUno = (numeroA * Xn.get(n-1)) % numeroM;
                Xn.add(numeroNmasUno);
                nroAleatorio.add(numeroNmasUno / numeroM);
            }
            System.err.println("Nro. Aleatorio Multiplicativo: " + (numeroNmasUno / numeroM));
        }
        return nroAleatorio;
    }
}

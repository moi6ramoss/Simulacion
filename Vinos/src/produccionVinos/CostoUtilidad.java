package produccionVinos;

/**
 *
 * @author mramoss
 */
public class CostoUtilidad {

    private String tipoVino;
    private Double vinoMadurado;
    private Double costoFijo;
    private Double costoVariable;
    private Double precioUnitarioVenta;

    public CostoUtilidad(String tipoVino, Double vinoMadurado, Double costoFijo,
            Double costoVariable, Double precioUnitarioVenta) {
        this.tipoVino = tipoVino;
        this.vinoMadurado = vinoMadurado;
        this.costoFijo = costoFijo;
        this.costoVariable = costoVariable;
        this.precioUnitarioVenta = precioUnitarioVenta;
    }

    public String getTipoVino() {
        return tipoVino;
    }

    public void setTipoVino(String tipoVino) {
        this.tipoVino = tipoVino;
    }

    public Double getVinoMadurado() {
        return vinoMadurado;
    }

    public void setVinoMadurado(Double vinoMadurado) {
        this.vinoMadurado = vinoMadurado;
    }

    public Double getCostoFijo() {
        return costoFijo;
    }

    public void setCostoFijo(Double costoFijo) {
        this.costoFijo = costoFijo;
    }

    public Double getCostoVariable() {
        return costoVariable;
    }

    public void setCostoVariable(Double costoVariable) {
        this.costoVariable = costoVariable;
    }

    public Double getPrecioUnitarioVenta() {
        return precioUnitarioVenta;
    }

    public void setPrecioUnitarioVenta(Double precioUnitarioVenta) {
        this.precioUnitarioVenta = precioUnitarioVenta;
    }
    
    public double costoVaribleTotal(){
     double costoVariable = this.vinoMadurado*this.costoVariable;
     return costoVariable;
    }
    
    public double totalVenta(){
        double totalVenta = vinoMadurado*precioUnitarioVenta;
        return totalVenta;
    }
    
    public double utilidad(){
        double utilidad = totalVenta()- costoVaribleTotal()- costoFijo;
        return utilidad;
    }
    public double porcentajeEstimado(){
        double porcentaje = 0.0;
        return porcentaje;
    }

}

package produccionVinos;

import generador.CongruencialMixto;
import generador.DistribucionSimulacion;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author mramoss
 */
public class MacFerMad {

    private String tipoVino;
    private Double uvasPorcesar;
    private double nroAleatorioTres;

    public MacFerMad(String tipoVino, Double uvasPorcesar, double nroAleatorioTres) {
        this.tipoVino = tipoVino;
        this.uvasPorcesar = uvasPorcesar;
        this.nroAleatorioTres = nroAleatorioTres;
    }

    public String getTipoVino() {
        return tipoVino;
    }

    public void setTipoVino(String tipoVino) {
        this.tipoVino = tipoVino;
    }

    public Double getUvasPorcesar() {
        return uvasPorcesar;
    }

    public void setUvasPorcesar(Double uvasPorcesar) {
        this.uvasPorcesar = uvasPorcesar;
    }

    public double getNroAleatorioTres() {
        return nroAleatorioTres;
    }

    public void setNroAleatorioTres(double nroAleatorioTres) {
        this.nroAleatorioTres = nroAleatorioTres;
    }



    public double porcentajeUvas() {
        double uvasKg = 0.0;
        if ("Espumantes".equals(getTipoVino())) {
            uvasKg = getUvasPorcesar() * 0.10;
        } else {
            uvasKg = getUvasPorcesar() * 0.90;
        }
        return uvasKg;
    }

    public int tiempoFermentacion() {
        Random numAleatorio = new Random();
        int nro = numAleatorio.nextInt(30 - 16 + 1) + 16;
        return nro;
    }

    public int rendimiento() {
        Random numAleatorio = new Random();
        int nro = numAleatorio.nextInt(830 - 750 + 1) + 750;
        return nro;
    }

    public double resultadoFermentacion() {
        double ltsFerementacion = 0.0;
        ltsFerementacion = (rendimiento() * porcentajeUvas()) / 1000;
        return ltsFerementacion;
    }

    public double nroAleatorio() { 
        DistribucionSimulacion newDistribucionSimulacion = new DistribucionSimulacion();
        double nroAleatorio = 0.0;
        nroAleatorio = newDistribucionSimulacion.generadorDistribucionUniformeAB(0.60,0.70,this.nroAleatorioTres);
        return nroAleatorio;
    }

    public double maduracion() {
        double maduracion = 0.0;
        maduracion = resultadoFermentacion() * nroAleatorio();
        return maduracion;
    }
}
